package com.strands.regex;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.strands.constants.RegularExpressions.*;

public class RegexTest {

    @Test
    public void EmailTest() {
        List<String> emails = new ArrayList();
        emails.add("f.soler@strands.com");
        emails.add("i.tarradellas@strands.com");
        emails.add("a.dereina@strands.com");

        Pattern pattern = Pattern.compile(EMAIL_REGEX);

        for(String email : emails){
            Matcher matcher = pattern.matcher(email);
            System.out.println(email +" : "+ matcher.matches());
        }
    }

    @Test
    public void StringAndDateAndDateExtractorTest() {
        List<String> strings = new ArrayList();
        strings.add("ELCORTEINGLES28/5/13");
        strings.add("CARREFOUR2/10/13");

        Pattern pattern;
        for(String string : strings){
            pattern = Pattern.compile(STRING_AND_DATE_REGEX);
            Matcher matcher = pattern.matcher(string);
            if (matcher.find()) {
                pattern = Pattern.compile(DATE_EXTRACTOR);
                matcher = pattern.matcher(string);
                if (matcher.find())
                    System.out.println(matcher.group(0));
            }
        }
    }
}
