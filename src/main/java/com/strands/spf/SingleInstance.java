package com.strands.spf;

/**
 * <p>
 * Single instance in a multi threading environment
 * </p>
 *
 * @author strands
 *
 */
public class SingleInstance {

    private static SingleInstance instance = null;

    private SingleInstance() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        SingleInstance a = SingleInstance.getInstance();
        SingleInstance b = SingleInstance.getInstance();
        System.out.println(a == b);
    }

    public static SingleInstance getInstance(){
        if (instance == null) {
            synchronized (SingleInstance.class) {
                if (instance == null) {
                    instance = new SingleInstance();
                }
            }
        }
        return instance;
    }

}


