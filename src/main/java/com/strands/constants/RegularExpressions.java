package com.strands.constants;

public class RegularExpressions {
  public static final String EMAIL_REGEX = "^[A-Za-z]\\.[A-Za-z]+@strands\\.+com$";
  public static final String STRING_AND_DATE_REGEX = "^[A-Z]+[0-9]+/[0-9]+/[0-9]+$";
  public static final String DATE_EXTRACTOR = "[0-9]+/[0-9]+/[0-9]+$";
}
