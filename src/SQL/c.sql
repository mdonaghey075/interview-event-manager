select a.USER_ID, a.NAME, a.ID, t.ID, t.CATEGORY_ID, t.NAME, t.AMOUNT, t.POSTED_DATE
from TRANSACTION t
inner join Account a
on t.ACCOUNT_ID = a.ID
WHERE a.USER_ID = 356789
AND MONTH(t.POSTED_DATE) = MONTH(getdate())
AND YEAR(t.POSTED_DATE) = YEAR(getdate())
AND t.AMOUNT > 0;